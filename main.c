//
//  main.c
//  Away From Keyboard auto subsync
//
//  Created by Aymeric Halé on 31/07/2020.
//  Copyright © 2020 Aymeric Halé. All rights reserved.
//
// Usage:
//
// Normal mode:      ./subsyncer movie subs.srt
// Details option:   ./subsyncer movie subs.srt -analysis
// Manual option:    ./subsyncer -manual subs.srt ±delay_ms
//
// Option syntax is order rigid.
//
// The manual mode allows you to create unsynced subs on purpose if you want to test the automatic syncing, ±delay_ms being the delay in milliseconds.
//
// Output of the automatic mode is subs_synced.srt, output of the manual mode is subs_manual_sync.srt, both are placed at the same path of input files. No overwrite warning is coded so eventually beware.
//
// TODO: different lengthed autocorrelation result / install in CMakeLists.txt / known bug: FFmpeg pipein extraction seems to stop at some point for some movies (>2hours?), buffer bytes are stuck at zero => output VAD is stuck at zero, but results before that are usable...
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <inttypes.h>

#include "fvad/fvad.h"
#include "kissfft/kiss_fftndr.h"
#include "kissfft/kiss_fft.h"
#include "kissfft/_kiss_fft_guts.h"

int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p);
size_t getMovieLength(char *moviePath);
size_t getSubNum(FILE *fileSubs);
void getSubFrames(float *frames, FILE *fileSubs);
void setSubList(float *sub, float *frames, size_t audioLen, size_t subNum);

void processVAD(char *moviePath, float *aud, Fvad *vad, size_t framelen, size_t audioLen);
void writeNewSubs(int frDelay, size_t subNum, float *frames, FILE *fileSubIn, FILE *fileSubOut);

enum {
    KISS_CONV = 0,
    KISS_XCORR,
};

void rfft_xcorr(size_t n, const kiss_fft_scalar *x, const kiss_fft_scalar *y, kiss_fft_scalar *z, int mode);
int find_max(const kiss_fft_scalar *dat, size_t N);


int main(int argc, char *argv[]) {
    
    printf("\nSubsyncer version 1.0\n\n");
    
    char *usage = "Usage:\n\nNormal mode:      ./subsyncer movie subs.srt\nDetails option:   ./subsyncer movie subs.srt -analysis\nManual option:    ./subsyncer -manual subs.srt ±delay_ms\n\nOption syntax is order rigid.\n\nThe manual mode allows you to create unsynced subs on purpose if you want to test the automatic syncing, ±delay_ms being the delay in milliseconds.\n\nOutput of the automatic mode is subs_synced.srt, output of the manual mode is subs_manual_sync.srt, both are placed at the same path of input files. No overwrite warning is coded so eventually beware.\n\n";
    
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);
    
    if (argc == 1) {
        printf("%s", usage);
        return 0;
    }
    
    if (strcmp(argv[1],"-manual") != 0) {
        FILE *checksMovie;
        checksMovie = fopen(argv[1], "rb");
        if (checksMovie == NULL) {
            printf("Movie not found at specified location, exit\n\n");
            printf("%s", usage);
            return 0;
        }
        fclose(checksMovie);
    }
    
    char *subs;
    subs = argv[2];
    FILE *fileSubs;
    fileSubs = fopen(subs, "rb");
    if (fileSubs == NULL) {
        printf(".srt file not found at specified location, exit\n\n");
        return 0;
    }
    
    size_t subNum = getSubNum(fileSubs);
    rewind(fileSubs);
    float *frameSubs = NULL;
    frameSubs=malloc(2*subNum * sizeof(float));
    
    if (frameSubs == NULL) {
        printf("Allocation impossible \n");
        exit(EXIT_FAILURE);
    }
    
    getSubFrames(frameSubs, fileSubs);
    rewind(fileSubs);
    
    if ( (argc == 3 && strcmp(argv[1], "-manual") != 0) || (argc == 4 && strcmp(argv[1],"-manual") != 0 && strcmp(argv[3], "-analysis") == 0) ) {
        
        float *subList = NULL;
        size_t audioLen = getMovieLength(argv[1]);
        if (audioLen%2==1) audioLen = audioLen-1;
        subList = malloc(audioLen * sizeof(float));
        setSubList(subList, frameSubs, audioLen, subNum);

        printf("----------------------\nAutomatic syncing mode\n----------------------\n\nAudio extraction and voice detection in progress...\nMovie length = 0%lu:%lu:%lu\n",(audioLen/100)/(60*60), ((audioLen/100)%(60*60))/60, (((audioLen/100)%(60*60))%60) );
        
        int frame_ms = 10;
        float *audioList = NULL;
        audioList = malloc(audioLen * sizeof(float));
        
        Fvad *vad = NULL;
        vad = fvad_new();
        if (!vad) printf("Out of memory \n");
        
        fvad_set_mode(vad, 3); // vad agressiveness: 0 is soft, 3 is hard
        fvad_set_sample_rate(vad, 48000);
        int framelen = 48000/1000 * frame_ms;
        processVAD(argv[1], audioList, vad, framelen, audioLen);
        float score = 0, speeched = 0, detected = 0;
        for (unsigned i=0; i<audioLen; i++) {
            score += (audioList[i] - subList[i])*(audioList[i] - subList[i]);
            speeched += subList[i];
            detected += audioList[i];
            //printf("%f %f \n", audioList[i], subList[i]);
        }
        score = 100*(1-score/audioLen);
        speeched = 100*speeched/audioLen;
        detected = 100*detected/audioLen;
        printf("%f%% of the subframes are speeched\n%f%% of the audioframes are detected as voices\nLet's make them match!\n\n", speeched, detected);
        
        if (vad) fvad_free(vad);
        
        struct timespec startFFT, endFFT;
        clock_gettime(CLOCK_MONOTONIC, &startFFT);
        int delay;
//        kiss_fft_scalar correlation[audioLen];
        kiss_fft_scalar *correlation = NULL;
        correlation = malloc(audioLen * sizeof(float));
        rfft_xcorr(audioLen, audioList, subList, correlation, KISS_XCORR);
        delay = find_max(correlation, audioLen);
        
        /*
        // New score calculation stuff, but meaningless. The score can be lower but still better match the audio with cross-correlation.
        int i;
        if (delay<0) {
            i = -delay;
        } else {
            i = 0;
        };
        score = 0;
        while (i+delay > 0 && i+delay < audioLen) {
            score += (audioList[i] - subList[i+delay])*(audioList[i] - subList[i+delay]);
            i += 1;
        }
        score = 100*(1-score/audioLen);
        printf("New score is %f% ", score);
         */
        
        clock_gettime(CLOCK_MONOTONIC, &endFFT);
        uint64_t timeFFTNano = timespecDiff(&endFFT, &startFFT);
        double timeFFT = timeFFTNano;
        timeFFT /= 1000000000;
        printf("Fourier executed in:      %f seconds\nOut of sync delay:        %dms\n", timeFFT, delay*10);

        
        FILE *fileNewSubs;
        char *newSubs = malloc(strlen(subs)+63*sizeof(char));
        strlcpy(newSubs, subs, strlen(subs)-3*sizeof(char));
        strlcat(newSubs, "_synced.srt", strlen(newSubs)+12*sizeof(char));
        printf("New subs are saved at:    %s\n", newSubs);
        
        fileNewSubs = fopen(newSubs, "wb");
        writeNewSubs(delay, subNum, frameSubs, fileSubs, fileNewSubs);
        
        if (correlation) free(correlation);
        correlation = malloc(audioLen * sizeof(float));

        
        if (argc == 4) {
            printf("\nDelay progressively found through audio, in milliseconds:\n");
            unsigned int i = 5;
            while (100*60*i < audioLen) {
                rfft_xcorr(100*60*i, audioList, subList, correlation, KISS_XCORR);
                delay = find_max(correlation, 100*60*i);
                printf("%dmin: %d\n", i, delay*10);
                i += 5;
            }
        }
        
        if (subList) free(subList);
        if (audioList) free(audioList);
        if (correlation) free(correlation);
        fclose(fileNewSubs);
        
    } else if (argc == 4 && strcmp(argv[1],"-manual") == 0) {
        printf("----------------------\nManual syncing mode\n----------------------\n\n");
        
        char *delay = argv[3];
        int delayLen = strlen(delay);
        if (strcmp(&delay[0], "-") || isdigit(delay[0])) {
            for (int i=1;i<delayLen; i++) {
                if (!isdigit(delay[i])) {
                    printf("Delay input is not a number, exit\n\n");
                    return 0;
                }
            }
        } else {
            printf("Delay input is not a number, exit\n\n");
        }
        
        char *p;
        FILE *fileNewSubs;
        char *newSubs = malloc(strlen(subs)+69*sizeof(char));
        strlcpy(newSubs, subs, strlen(subs)-3*sizeof(char));
        strlcat(newSubs, "_manual_sync.srt", strlen(newSubs)+18*sizeof(char));
        fileNewSubs = fopen(newSubs, "wb");
        writeNewSubs(strtol(argv[3], &p, 10)/10, subNum, frameSubs, fileSubs, fileNewSubs);
        fclose(fileNewSubs);
        printf("Shifting delay specified: %sms\nNew subs are saved at:    %s\n", argv[3], newSubs);
        
    } else {
        printf("%s",usage);
        return 0;
    }
    
    fclose(fileSubs);
    
    clock_gettime(CLOCK_MONOTONIC, &end);
    uint64_t timeNanoElapsed = timespecDiff(&end, &start);
    double timeElapsed = timeNanoElapsed;
    timeElapsed = timeElapsed / 1000000000;
    sleep(1);
    printf("\nTotal execution time:     %fs\nThanks for using Subsyncer :)\n\n", timeElapsed);
    
    return 0;
}

int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p) {
  return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) -
           ((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
}

size_t getMovieLength(char *moviePath) {
    char *p;
    char *len = malloc(2*sizeof(size_t));
    char *cmd = malloc(strlen(moviePath)+65*sizeof(char));
    strlcpy(cmd, "ffprobe -i ", 12*sizeof(char));
    strlcat(cmd, moviePath, strlen(cmd)+strlen(moviePath)+1*sizeof(char));
    strlcat(cmd, " -show_entries format=duration -v quiet -of csv=\"p=0\"", strlen(cmd)+54*sizeof(char));
//    printf("%s \n", cmd);
    
    FILE *pipe;
    pipe = popen(cmd, "r");
    fgets(len, 2*sizeof(size_t), pipe);
    pclose(pipe);
    size_t audioLen = atof(len)*100;
    
    return audioLen;
    // returns movie length in multiple of 10ms frames
}

size_t getSubNum(FILE *fileSubs)
{
    char *line = NULL;
    size_t len = 0;
    size_t num = 0;
    while (getline(&line, &len, fileSubs) != -1) {
        if (strstr(line,("-->"))!=NULL) {
            num+=1;
        }
    }
    return num;
}

void getSubFrames(float *frames, FILE *fileSubs)
{
    char *line = NULL;
    size_t len = 0;
    int num = 0;

    while (getline(&line, &len, fileSubs) != -1) {
        if (strstr(line,("-->"))!=NULL) {
            frames[2*num] = (float) ((line[0]-'0')*10+(line[1]-'0'))*60*60*100 + ((line[3]-'0')*10+(line[4]-'0'))*60*100 + ((line[6]-'0')*10+(line[7]-'0'))*100+(line[9]-'0')*10+(line[10]-'0');

            frames[2*num+1] = (float) ((line[17]-'0')*10+(line[18]-'0'))*60*60*100 + ((line[20]-'0')*10+(line[21]-'0'))*60*100 + ((line[23]-'0')*10+(line[24]-'0'))*100+(line[26]-'0')*10+(line[27]-'0');
            num+=1;
        }
    }

    if (line) {
        free(line);
    }
}

void setSubList(float *sub, float *frames, size_t audioLen, size_t subNum) {
    int num = 0;
    for (unsigned i=0; i<audioLen; i++) {
        if (i >= frames[2*num] && i <= frames[2*num+1] && num<=subNum) {
            sub[i] = 1.0;
            if (i == frames[2*num+1]) {
                num += 1.0;
            }
        } else {
            sub[i] = 0.0;
        }
    }
}

void processVAD(char *moviePath, float *aud, Fvad *vad, size_t framelen, size_t audioLen)
{
    struct timespec startExtract, endExtract;
    struct timespec startVAD, endVAD;

    char *full_cmd = malloc(strlen(moviePath)+63*sizeof(char));
    strlcpy(full_cmd, "ffmpeg -loglevel quiet -stats -i ", 34*sizeof(char));
    strlcat(full_cmd, moviePath, strlen(full_cmd)+strlen(moviePath)+1*sizeof(char));
    strlcat(full_cmd, " -f s16le -ar 48000 -ac 1 -", strlen(full_cmd)+28*sizeof(char));
    //printf("%s \n", full_cmd);

    clock_gettime(CLOCK_MONOTONIC, &startExtract);

    int16_t *buf = NULL;
    buf = malloc(framelen * sizeof *buf);
    FILE *pipein;
    pipein = popen(full_cmd, "r");

    int vadResult = -1;
    size_t lastDetect=  0;
    size_t numFrame = 0;
    uint64_t timeVadNano = 0;
    
    // while alternative but we don't want to overflow fixed arrays allocation
    /*    while (fread(buf, 2, framelen, pipein) == framelen) { */
    
    while (numFrame < audioLen) {
        
        fread(buf, 2, framelen, pipein);
        clock_gettime(CLOCK_MONOTONIC, &startVAD);
        vadResult = fvad_process(vad, buf, framelen);
        clock_gettime(CLOCK_MONOTONIC, &endVAD);
        
        timeVadNano += timespecDiff(&endVAD, &startVAD);
        if (vadResult == 1) lastDetect = numFrame;
        aud[numFrame] = (float) vadResult;
        numFrame += 1;
//        printf("%zu %d \n", numFrame, vadResult);
//        printf("%zu %hd \n", numFrame, buf[0]);
        if (vadResult < 0) {
            printf("VAD processing failed\n");
        }
    }
    
    clock_gettime(CLOCK_MONOTONIC, &endExtract);
    uint64_t timeExtractNano = timespecDiff(&endExtract, &startExtract);
    double timeExtract = timeExtractNano;
    timeExtract /= 1000000000;
    double timeVAD = timeVadNano;
    timeVAD /= 1000000000;
    printf("\nExtraction done in:       %f seconds\nVAD analysis done in:     %f seconds\nLast detected voice at:   0%lu:%lu:%lu\n", timeExtract-timeVAD, timeVAD, (lastDetect/100)/(60*60), ((lastDetect/100)%(60*60))/60, (((lastDetect/100)%(60*60))%60));
//    if (lastDetect < 3*audioLen/4) printf("Extraction possibly incomplete\n");
    printf("\n");
    
    if (buf) free(buf);
    pclose(pipein);
}


void writeNewSubs(int frDelay, size_t subNum, float *frames, FILE *fileSubIn, FILE *fileSubOut)
{
    char *line = NULL;
    size_t len = 0;
    int newFrame = 0;
    int num = 0;
    int hh = 0, mm = 0, ss = 0, cs = 0;

    while (getline(&line, &len, fileSubIn) != -1) {
/*        printf("%s",line); */
//        printf("%s", line);

        if (strstr(line,("-->"))!=NULL) {
            newFrame = frames[2*num] + frDelay;
            hh = (int)(newFrame / 360000L);
            newFrame %= 360000L;
            mm = (int)(newFrame / 6000);
            newFrame %= 6000;
            ss = (int)(newFrame / 100);
            newFrame %= 100;
            cs = (int) newFrame;
            
            line[0] = hh/10 + '0';
            line[1] = hh%10 + '0';
            line[3] = mm/10 + '0';
            line[4] = mm%10 + '0';
            line[6] = ss/10 + '0';
            line[7] = ss%10 + '0';
            line[9] = cs/10 + '0';
            line[10] = cs%10 + '0';
            
            newFrame = frames[2*num+1] + frDelay;
            hh = (int)(newFrame / 360000L);
            newFrame %= 360000L;
            mm = (int)(newFrame / 6000);
            newFrame %= 6000;
            ss = (int)(newFrame / 100);
            newFrame %= 100;
            cs = (int) newFrame;
            
            line[17] = hh/10 + '0';
            line[18] = hh%10 + '0';
            line[20] = mm/10 + '0';
            line[21] = mm%10 + '0';
            line[23] = ss/10 + '0';
            line[24] = ss%10 + '0';
            line[26] = cs/10 + '0';
            line[27] = cs%10 + '0';
            
//            printf("%s", line);

            num += 1;
        }
        fprintf(fileSubOut, "%s", line);
    }
    
    if (line) {
        free(line);
    }
    
}

void rfft_xcorr(size_t n, const kiss_fft_scalar *x, const kiss_fft_scalar *y,
        kiss_fft_scalar *z, int mode) {
    int freq_len = n / 2 + 1;
    int i;

    // FFT configs
    kiss_fftr_cfg fft_fwd = kiss_fftr_alloc(n, 0, NULL, NULL );
    kiss_fftr_cfg fft_bwd = kiss_fftr_alloc(n, 1, NULL, NULL );

    // Freq-domain data buffer
    kiss_fft_cpx *X = calloc(freq_len, sizeof(kiss_fft_cpx));
    kiss_fft_cpx *Y = calloc(freq_len, sizeof(kiss_fft_cpx));
    kiss_fft_cpx *Z = calloc(freq_len, sizeof(kiss_fft_cpx));

    // Execute FWD_FFT
    kiss_fftr(fft_fwd, x, X);
    kiss_fftr(fft_fwd, y, Y);

    // Multiply in freq-domain
    for (i = 0; i < freq_len; i++) {
        if (mode == KISS_XCORR) {
            // take conjugate of Y
            Y[i].i = -Y[i].i;
        }
        C_MUL(Z[i], X[i], Y[i]);
    }

    // Execute BWD_FFT
    kiss_fftri(fft_bwd, Z, z);

    free(fft_fwd);
    free(fft_bwd);

    free(X);
    free(Y);
    free(Z);
}

int find_max(const kiss_fft_scalar *dat, size_t N) {
    kiss_fft_scalar max_v = -100000.0;
    int max_i = -1;
    int i;

    for (i = 0; i < N; i++) {
        if (dat[i] > max_v) {
            max_i = i;
            max_v = dat[i];
        }
    }
    
    if (max_i > N-max_i) max_i = max_i - N;
    return max_i;
}
