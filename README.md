# Subsyncer

#### A command line tool to automatically sync subtitles with a movie

- *Are you troubled by out-of-sync subtitles in the middle of the night?*
- *Do you experience feelings of dread when trying to sync them manually?*
- *Have you ever stopped watching an underground movie because you couldn't find its accurate subtitles?*
- *If the answer is "yes", then don't wait another minute. Pick up the git and clone up...Subsyncer!*

Subsyncer is a command line tool to automatically sync `.srt` subtitles with a movie audio, being a `.mp4`or almost any format thanks to FFmpeg versatility.

It is thought to be an intermediate step before adding the functionality to VLC. In this regard it is more designed to be an academic exercise and the piece of evidence that automatic syncing is achievable.

FFmpeg & FFprobe are the only required dependencies (alongside CMake and a C compiler).

## How it works

Raw audio is extracted from the movie through a system call to FFmpeg. The raw audio is sampled at 48kHz mono pcm_s16le. In the same time, the WebRTC VAD (Voice Activity Detection) algorithm analyses the presence of voices in 10ms frames. Results are kept in an array.

A same-length array is established from the subs, and the real Fourier transformation of both is calculated to process a fast cross-correlation. The out-of-sync delay is the index of the maximum value in the cross-correlation array resulted. The Fourier library used is KissFFT, for its lightweight and satisfying efficiency for even lengthed arrays.

And...that's all! An output file is rewritten according to the delay found.

## Build

The project uses CMake to produce the binary. It should work on every UNIX system with POSIX support like Debian, Ubuntu or macOS.

```c
git clone https://code.videolan.org/aym_h/subsyncer.git/
cd subsyncer
mkdir build && cd build
cmake ..
make
```

Installation is not featured as the tool is only intended to be an intermediate step before VLC implementation. In fact I don't believe a specific command line tool is that practical to use for subtitles syncing, but I may code executables with GUI later, for academic learning.

## Usage

`./subsyncer movie.mp4 subs.srt `

The command above produces the output `subs_synced.srt` at the same path of the input files. The option `-analysis` displays the out-of-sync delay calculated throughout the audio analysis. Syntax is order rigid and is as follows:

 `./subsyncer movie.mp4 subs.srt -analysis`

Lastly, a third mode allows syncing with a specified delay in milliseconds. It produces the output `subs_manual_sync.srt`. Again, syntax is order rigid:

`./subsyncer -manual subs.srt ±delay`

There is no overwrite warnings when output subs are written so eventually beware.

## Test & performances

If you test the algorithm on a movie with already accurate subtitles, you can produce out-of-sync subs on purpose with the manual mode:

```
./subsyncer -manual subs.srt ±random_delay
./subsyncer movie.mp4 subs_manual_sync.srt -analysis
```

If everything goes well the final output file *subs_manual_sync_synced.srt* should be fine!

**The `-analysis`mode is interesting to know *how soon* the algorithm finds out the accurate delay.** From tests, it can either be 20 or 45 minutes.

Typical precision of the output is about 0-200ms compared to the original synced subtitles, which is precise enough to go unnoticed.

The execution time of Subsyncer is about 5 to 40 seconds according to the CPU's load and the movie's length, extracting the audio being the longer process. 

**If restricted to a 20-minutes portion of movie, the VAD and Fourier calculation perform in about one second which comes really close to real-time syncing!** The issue being were 20 minutes enough for the algorithm to find out the accurate out-of-sync delay?

## VLC coming implementation

Before implementing the solution in VLC, I will ran the code on a wider panel of movies. The option in VLC should be convenient, fast and precise. There are two different approaches to explore :

- The user calls the syncing function in the subtitles settings, and the algorithm processes using the actual audio buffer (if at least 20 minutes long), or extracts a new one. Two to three seconds of calculation can be expected.
- When a module is activated, the syncing function runs in the background and adjusts newly detected shifts in the subs, likely caused by advertisement breaks.

**What's interesting is that VLC's *watch usage in real time* adresses the main concern this command line tool does not handle: advertisement breaks.** If the subs are synced on the 20 first minutes of the movie but go out of sync later, then the algorithms just needs to be run on the new 20 upcoming minutes.

## License & dependencies

`Copyright © 2020 Aymeric Halé. All rights reserved.`

Subsyncer is not open-source licensed, hence in legal terms I "own" the copyright of the small piece of code in the `main.c`, but it is nothing more than timecode interaction with `.srt` subtitles and library usage. If you'd like to use the whole code please contact me to request an open source license.

Subsyncer relies on two open-source libraries: [WebRTC](https://chromium.googlesource.com/external/webrtc/+/refs/heads/master/) and [KissFFT](https://github.com/mborgerding/kissfft), both under the BSD License clause. Original licenses can be found in their respective folders `subsyncer/kissfft` and `subsyncer/fvad`.

The WebRTC's VAD implementation I used is a practical fork made by [Daniel Pirch](https://github.com/dpirch/libfvad). The real cross-correlation function based on KissFFT was written by [Shiqiao Du](https://github.com/lucidfrontier45/kiss_xcorr).

The [FFmpeg](https://ffmpeg.org) system call does not involve any piece of its source code, hence no license is required.

## Acknowledgement

I'm grateful to J-B Kempf who tutored the internship which granted me time to improve my C programming skills. I learnt a lot and enjoyed engaging myself in the hacker world :) 

I hope this piece of code will be useful!

## Contact

- Mail: [aymeric.hale@institutoptique.fr](mailto:aymeric.hale@institutoptique.fr)
- Videolan's Gitlab profile: https://code.videolan.org/aym_h
- Please contact me if you require an open-source license

